# Django-Security-Automation

This is a test project to gauge the viability of a full-stack build using Django to automate the security testing of various CMS's security headers, cookies, and front-end vulnerabilities layered with a user-friendly interface. 

This repo includes the scaffolding for running this Django stack in VSCode from a Docker container. To begin, you will need to clone the repo and
then create your development database by running the command `python manage.py migrate`

>> From the VSCode Docs: 
> When you run the server the first time, it creates a default SQLite database in the file db.sqlite3 that is intended for development purposes, but can be used in  production for low-volume web apps.

Spin up your container and run `python manage.py runserver` to see it running on your localhost port. The default port is 8000, however you can also specify the port with `python manage.py runserver xxxx` exchanging the "xxxx" for desired port number. 
